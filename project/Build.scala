import sbt._
import Keys._

object EdSnakeBuild extends Build {
  lazy val root = Project(id = "root", base = file(".")) aggregate(edsnake, reinforcementLearningCore)
  
  lazy val edsnake = Project(id = "edsnake",  base = file("edsnake")) dependsOn(reinforcementLearningCore)
  
  lazy val reinforcementLearningCore = Project(id = "recore", base = file("core"))
}