name := "root"

version in ThisBuild := "1.0"

scalaVersion in ThisBuild := "2.10.2"

EclipseKeys.withSource in ThisBuild := true

EclipseKeys.createSrc in ThisBuild := EclipseCreateSrc.Default + EclipseCreateSrc.Resource

scalacOptions in ThisBuild ++= Seq("-unchecked", "-deprecation", "-feature")

libraryDependencies in ThisBuild += "org.scalatest" %% "scalatest" % "2.0.M5b" % "test"

libraryDependencies in ThisBuild += "com.google.guava" % "guava" % "14.0-rc1"

libraryDependencies in ThisBuild += "org.apache.commons" % "commons-lang3" % "3.1"

libraryDependencies in ThisBuild += "com.google.code.findbugs" % "jsr305" % "2.0.1"

libraryDependencies in ThisBuild += "org.slf4j" % "slf4j-api" % "1.7.3"

libraryDependencies in ThisBuild += "ch.qos.logback" % "logback-classic" % "1.0.10"

libraryDependencies in ThisBuild += "com.typesafe" %% "scalalogging-slf4j" % "1.0.1"
