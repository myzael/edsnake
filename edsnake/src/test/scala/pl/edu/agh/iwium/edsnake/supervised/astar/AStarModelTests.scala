package pl.edu.agh.iwium.edsnake.supervised.astar

import org.scalatest.FunSpec
import org.scalatest.matchers.ShouldMatchers

class AStarModelTests extends FunSpec with ShouldMatchers {

  private val model = new AStarModel()
  
  describe("AStar Model") {
    it("should give -1 to negative resources") {
      val farTest = model.builder.distance(10).life(19).value(-1).buildTest
      val nearTest = model.builder.distance(2).life(19).value(-1).buildTest
      
      val far = model.distribution(farTest)
      val near = model.distribution(nearTest)
      
      far(3) should be < (-0.9)
      near(3) should be < (-0.9)
    }
    
    it("should give 0 to high value and near resources") {
      val test = model.builder.distance(3).life(20).value(7).buildTest
      
      val priority = model.distribution(test)
      
      priority(3) should be <= (0.1)
      priority(0) should be > (0.8)
    }
  }
}