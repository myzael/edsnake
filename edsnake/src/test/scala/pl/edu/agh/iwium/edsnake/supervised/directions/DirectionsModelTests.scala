package pl.edu.agh.iwium.edsnake.supervised.directions

import org.scalatest.FunSpec
import org.scalatest.matchers.ShouldMatchers
import com.typesafe.scalalogging.slf4j.Logging

class DirectionsModelTests extends FunSpec with ShouldMatchers with Logging {

  private val move1 = new DirectionsModel("move1")

  logger.info("\n{}", move1)

  describe("Directions Model") {
    it("should give good direction for first move") {
      logger.info("right 1 down 0: {}", move1.moveDistribution(DirectionsModel.builder.right(1).down(0).clazz("+").buildTest))
      logger.info("right -1 down 0: {}", move1.moveDistribution(DirectionsModel.builder.right(-1).down(0).clazz("+").buildTest))
      logger.info("right 0 down 1: {}", move1.moveDistribution(DirectionsModel.builder.right(0).down(1).clazz("+").buildTest))
      logger.info("right 0 down -1: {}", move1.moveDistribution(DirectionsModel.builder.right(0).down(-1).clazz("+").buildTest))

      logger.info("right 1 down 1: {}", move1.moveDistribution(DirectionsModel.builder.right(1).down(1).clazz("+").buildTest))
      logger.info("right 2 down 3: {}", move1.moveDistribution(DirectionsModel.builder.right(2).down(3).clazz("+").buildTest))
      logger.info("right 10 down -11: {}", move1.moveDistribution(DirectionsModel.builder.right(10).down(-11).clazz("+").buildTest))
    }
  }
}