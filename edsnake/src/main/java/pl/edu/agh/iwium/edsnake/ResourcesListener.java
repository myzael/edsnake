package pl.edu.agh.iwium.edsnake;

public interface ResourcesListener {

	void resourceGenerated(Resource resource);

	void resourceExpired(Resource resource);
	
	void resourceEaten(Resource resource);
}
