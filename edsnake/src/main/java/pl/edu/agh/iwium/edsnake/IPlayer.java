package pl.edu.agh.iwium.edsnake;

public interface IPlayer {

	
	public void learn();
	
	public IStrategy getStrategy();

	public void updateBoard(Board board);

	public void awardPoints(int points);

	public int getResult();

	public Board getBoard();

	public void finishGame();
}