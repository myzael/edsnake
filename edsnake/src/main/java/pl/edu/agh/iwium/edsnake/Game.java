package pl.edu.agh.iwium.edsnake;

import java.io.File;

import org.newdawn.slick.Animation;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

import com.google.common.base.Joiner;

public class Game extends BasicGame {

	private TiledMap graphicalMap;
	private Animation sprite, up, down, left, right;
	private int x = 0, y = 0;
	private Board board;
	private static final int SIZE = 75;
	private final IPlayer player;
	private final int moveLimit;
	private int moves;
	private Image image;
	private final int delay;
	private int counter;

	public Game(IPlayer player, int moveLimit, int delay) {
		super("Game");
		this.player = player;
		this.moveLimit = moveLimit;
		this.delay = delay;
		this.moves = 0;
		this.counter = 0;
	}

	@Override
	public void render(GameContainer container, Graphics g) throws SlickException {
		graphicalMap.render(0, 0);
		sprite.draw(x, y);
		g.drawString(String.format("Result: %s Round: %s", player.getResult(), moves), 0, 0);

		drawResources(g);

		// Font font1 = new Font("Arial", Font.BOLD, 20);
		// TrueTypeFont font = new TrueTypeFont(font1 , true);
		// font.drawString(10, 10,"asdfasdf", Color.green);
	}

	private void drawResources(Graphics g) {
		for (Position p : board.getResourcesMap().keySet()) {
			int x = SIZE * p.getX();
			int y = SIZE * p.getY();
			Resource r = board.getResourcesMap().get(p);
			g.drawImage(image, x, y);
			g.drawString(String.format("%s/%s", r.getValue(), r.getLife()), x, y);
		}
	}

	@Override
	public void init(GameContainer container) throws SlickException {
		createSpritesAndAnimation();

		readAndCreateMap();

		updateBoard();
	}

	private void createSpritesAndAnimation() throws SlickException {
		Image[] movementUp = { new Image("img/up1.png"), new Image("img/up2.png") };
		Image[] movementDown = { new Image("img/down1.png"), new Image("img/down2.png") };
		Image[] movementLeft = { new Image("img/left1.png"), new Image("img/left2.png") };
		Image[] movementRight = { new Image("img/right1.png"), new Image("img/right2.png") };

		graphicalMap = new TiledMap("img/grassmap.tmx");

		/*
		 * false variable means do not auto update the animation. By setting it
		 * to false animation will update only when the user presses a key.
		 */
		int[] duration = { 200, 200 };
		up = new Animation(movementUp, duration, false);
		down = new Animation(movementDown, duration, false);
		left = new Animation(movementLeft, duration, false);
		right = new Animation(movementRight, duration, false);

		// Original orientation of the sprite.
		sprite = right;

		image = new Image("img/res.png");
	}

	private void readAndCreateMap() {

		boolean[][] blocked = new boolean[graphicalMap.getWidth()][graphicalMap.getHeight()];
		for (int xAxis = 0; xAxis < graphicalMap.getWidth(); xAxis++) {
			for (int yAxis = 0; yAxis < graphicalMap.getHeight(); yAxis++) {
				int tileID = graphicalMap.getTileId(xAxis, yAxis, 0);
				String value = graphicalMap.getTileProperty(tileID, "blocked", "false");
				if ("true".equals(value)) {
					blocked[xAxis][yAxis] = true;
				}
			}
		}

		board = new Board(blocked);
	}

	@Override
	public void update(GameContainer container, int delta) throws SlickException {
		counter += delta;
		if (counter >= delay) {
			exitOnMoveLimitExceeded(container);
			move(player.getStrategy().getMove());
			moves++;
			updateBoard();
			counter = 0;
		}
	}

	private void updateBoard() {
		board.setPlayerPosition(x / SIZE, y / SIZE);
		board.updateBoard();
		player.updateBoard(board);
	}

	private void exitOnMoveLimitExceeded(GameContainer container) {
		if (moves > moveLimit) {
			System.out.println(player.getResult());
			player.finishGame();
			container.exit();
		}
	}

	private void move(Move move) {
		if (Move.UP.equals(move)) {
			sprite = up;
			if (!isBlocked(x, y - SIZE)) {
				sprite.update(SIZE);
				y -= SIZE;
				handleResource();
			}
		} else if (Move.DOWN.equals(move)) {
			sprite = down;
			if (!isBlocked(x, y + SIZE)) {
				sprite.update(SIZE);
				y += SIZE;
				handleResource();
			}
		} else if (Move.LEFT.equals(move)) {
			sprite = left;
			if (!isBlocked(x - SIZE, y)) {
				sprite.update(SIZE);
				x -= SIZE;
				handleResource();
			}
		} else if (Move.RIGHT.equals(move)) {
			sprite = right;
			if (!isBlocked(x + SIZE, y)) {
				sprite.update(SIZE);
				x += SIZE;
				handleResource();
			}
		}
	}

	private void handleResource() {
		if (isResource(x, y)) {
			consumeResource(x, y);
		} else {
			board.setLastReward(0);
		}
	}

	private void consumeResource(int x, int y) {
		int xBlock = x / SIZE;
		int yBlock = y / SIZE;
		int points = board.consumeResource(xBlock, yBlock);
		board.setLastReward(points);
		player.awardPoints(points);
	}

	private boolean isBlocked(float x, float y) {
		int xBlock = (int) x / SIZE;
		int yBlock = (int) y / SIZE;
		if (xBlock < 0 || yBlock < 0 || xBlock >= board.getWidthInTiles() || yBlock >= board.getHeightInTiles())
			return true;
		return board.blocked(xBlock, yBlock);
	}

	private boolean isResource(float x, float y) {
		int xBlock = (int) x / SIZE;
		int yBlock = (int) y / SIZE;
		return board.isResource(xBlock, yBlock);
	}

	public static void main(String[] arguments) throws Exception {
		run(new Automaton(), 1000, 0);
	}

	public static void run(IPlayer player, int moveLimit, int delay) throws Exception {
		loadLibraries();
		Game game = new Game(player, moveLimit, delay);


		AppGameContainer app = new AppGameContainer(game);
		app.setDisplayMode(SIZE * 10, SIZE * 10, false);
		app.setTargetFrameRate(60);
		app.setVSync(true);
		app.start();
	}
	

	private static void loadLibraries() {
		Joiner joiner = Joiner.on(File.separator);

		String systemNativLibraryDirectory = getSystemNativLibraryDirectory();
		String pathA = joiner.join("lib", "lwjgl-2.9.0", "native", systemNativLibraryDirectory);
		String pathB = joiner.join("edsnake", pathA);
		File possibleLibrayPathA = new File(pathA);
		File possibleLibrayPathB = new File(pathB);

		File correctPath = possibleLibrayPathA.exists() ? possibleLibrayPathA : possibleLibrayPathB;
		System.setProperty("org.lwjgl.librarypath", correctPath.getAbsolutePath());
	}

	private static String getSystemNativLibraryDirectory() {
		String osname = System.getProperty("os.name").toLowerCase();
		if (osname.indexOf("win") >= 0) {
			return "windows";
		} else {
			return "linux";
		}
	}
}
