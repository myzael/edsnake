package pl.edu.agh.iwium.edsnake.learning.reinforcement;

public enum RewardSize {
	NONE(0), POSITIVE(1), NEGATIVE(-1), VERY_POSITIVE(2), VERY_NEGATIVE(-2);
	
	private int points;
	
	private RewardSize(int points) {
		this.points = points;
	}
	
	public int getPoints() {
		return points;
	}

}
