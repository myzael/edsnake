package pl.edu.agh.iwium.edsnake.learning.reinforcement;

import java.util.Arrays;

import pl.edu.agh.iwium.edsnake.Move;
import environment.AbstractState;
import environment.IEnvironment;
import environment.IState;

public class EdSnakeState extends AbstractState {

	private PiqleBoard environment;
	
	private RewardSize[] stepRewards;
	
	private int x;
	
	private int y;
	
	
	public EdSnakeState(IEnvironment ct, RewardSize[] rewards, int x, int y) {
		super(ct);
		this.environment = (PiqleBoard)ct;
		stepRewards = rewards;
		this.x = x;
		this.y = y;
	}

	public int getXPosition() {
		return x;
	}
	
	public int getYPosition() {
		return y;
	}
	

	public RewardSize getRewardForMove(Move move) {
		return stepRewards[move.ordinal()];
	}
	
	private RewardSize[] getPossibleRewards() {
		return stepRewards;
	}

	private static final long serialVersionUID = -7412665026942918352L;

	@Override
	public IState copy() {
		return new EdSnakeState(environment, stepRewards.clone(),x,y);
	}

	public int nnCodingSize() {
		return stepRewards.length*RewardSize.values().length;
	}

	public double[] nnCoding() {
		double code[] = new double[nnCodingSize()];
		
		int shift = 0;
		for (RewardSize r: stepRewards) {
			code[r.ordinal()+shift] = 1.0;
			shift += RewardSize.values().length;
			
		}		
		return code;
	}
	
	@Override
	public int hashCode() {
		int hash = 0;
		int position = 1;
		for(RewardSize rewardSize : stepRewards) {
			hash += position*rewardSize.getPoints();
			position *= 10;
		}
		return hash;
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof EdSnakeState))
			return false;
		EdSnakeState state = (EdSnakeState) o;
		return Arrays.equals(this.stepRewards,state.getPossibleRewards());
	}

}
