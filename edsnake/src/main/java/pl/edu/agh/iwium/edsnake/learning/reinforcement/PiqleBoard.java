package pl.edu.agh.iwium.edsnake.learning.reinforcement;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import pl.edu.agh.iwium.edsnake.Board;
import pl.edu.agh.iwium.edsnake.Move;
import pl.edu.agh.iwium.edsnake.Position;
import pl.edu.agh.iwium.edsnake.Resource;
import environment.AbstractEnvironmentSingle;
import environment.ActionList;
import environment.IAction;
import environment.IState;

public class PiqleBoard extends AbstractEnvironmentSingle {

	private static final long serialVersionUID = 923965896700117038L;
	private Board board;
	private int points;

	private boolean[][] blocked;

	private Bouton grafTable[][];

	public PiqleBoard(boolean[][] blocked) {
		this.blocked = blocked;
		this.clearBoard();
	}

	public int getXPosition() {
		return board.getPlayerPostition().getX();
	}

	public int getYPosition() {
		return board.getPlayerPostition().getY();
	}

	public void clearBoard() {
		points = 0;
		board = new Board(blocked);
	}

	public int getPoints() {
		return points;
	}

	@Override
	public ActionList getActionList(IState s) {
		EdSnakeState state = (EdSnakeState) s;
		ActionList actions = new ActionList(state);
		int x = state.getXPosition();
		int y = state.getYPosition();

		for (Move move : Move.values()) {
			if (!board.blocked(x + move.getXMove(), y + move.getYMove())) {
				actions.add(new EdSnakeAction(move));
			}
		}

		return actions;
	}

	@Override
	public IState successorState(IState s, IAction a) {
		EdSnakeState state = (EdSnakeState) s;
		EdSnakeAction action = (EdSnakeAction) a;

		int newX = state.getXPosition() + action.getMove().getXMove();
		int newY = state.getYPosition() + action.getMove().getYMove();
		RewardSize[] rewardTable = new RewardSize[Move.values().length];
		for (Move move : Move.values()) {
			rewardTable[move.ordinal()] = getRewardForLine(newX, newY, move);
		}

		return new EdSnakeState(this, rewardTable, newX, newY);
	}

	private RewardSize getRewardForLine(int x, int y, Move move) {
		x += move.getXMove();
		y += move.getYMove();
		if (!board.blocked(x, y) && board.isResource(x, y)) {
			int intReward = board.getResourcesMap().get(new Position(x, y))
					.getValue();
			if (intReward < 0) {
				return RewardSize.VERY_NEGATIVE;
			} else {
				return RewardSize.VERY_POSITIVE;
			}
		}
		while (!board.blocked(x, y)) {
			if (board.isResource(x, y)) {
				int intReward = board.getResourcesMap().get(new Position(x, y))
						.getValue();
				if (intReward < 0) {
					return RewardSize.NEGATIVE;
				} else {
					return RewardSize.POSITIVE;
				}
			}		
			x += move.getXMove();
			y += move.getYMove();
		}
		return RewardSize.NONE;
	}

	@Override
	public IState defaultInitialState() {

		int x, y;
		do {
			x = (int) (Math.random() * board.getWidthInTiles());
			y = (int) (Math.random() * board.getHeightInTiles());
		} while (board.blocked(x, y) && !board.isResource(x, y));

		RewardSize[] rewardTable = new RewardSize[Move.values().length];
		for (Move move : Move.values()) {
			rewardTable[move.ordinal()] = getRewardForLine(x, y, move);
		}
		return new EdSnakeState(this, rewardTable, x, y);
	}

	@Override
	public double getReward(IState s1, IState s2, IAction a) {
		EdSnakeState lastState = (EdSnakeState) s1;
		EdSnakeAction action = (EdSnakeAction) a;
		return lastState.getRewardForMove(action.getMove()).getPoints();
	}

	@Override
	public boolean isFinal(IState s) {
		return false;
	}

	@Override
	public int whoWins(IState s) {
		return 0;
	}

	public void updateBoard(IState currentState) {
		EdSnakeState state = (EdSnakeState) currentState;
		
		if (board.isResource(state.getXPosition(), state.getYPosition())) {
			points += board.consumeResource(state.getXPosition(),
					state.getYPosition());
		}

		board.setPlayerPosition(state.getXPosition(), state.getYPosition());
		board.updateBoard();

	}

	/* Graphical methods */

	// 0 - if nothing, 1 - if wall, 2 - if resource
	private int getCellType(int x, int y) {
		if (board.blocked(x, y)) {
			return 1;
		}

		if (board.isResource(x, y)) {
			
			Resource res = board.getResourcesMap().get(new Position(x, y));
			
			if (res.getValue() >= 0) {
				return 2;
			} else {
				return 3;
			}
		}

		return 0;
	}

	protected JFrame graf;

	@Override
	public void setGraphics() {
		graf = new JFrame();
		graf.setSize(new Dimension(40 * board.getWidthInTiles(), 40 * board
				.getHeightInTiles()));
		// TODO revoir le comportement du programme en cas de quittage
		graf.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.out.println("On quitte");
				System.exit(0);
			}
		});
		graf.getContentPane().setLayout(
				new GridLayout(board.getWidthInTiles(), board
						.getHeightInTiles()));

		grafTable = new Bouton[board.getWidthInTiles()][];
		for (int i = 0; i < board.getWidthInTiles(); i++) {
			grafTable[i] = new Bouton[board.getHeightInTiles()];
			for (int j = 0; j < board.getHeightInTiles(); j++) {
				grafTable[i][j] = new Bouton(getCellType(i, j));
			}
		}
		for (int i = 0; i < board.getWidthInTiles(); i++)
			for (int j = 0; j < board.getHeightInTiles(); j++)
				graf.getContentPane().add(grafTable[i][j]);
		graf.setVisible(true);

	}

	@Override
	public void closeGraphics() {
	}

	@Override
	public void sendState(IState e) {
		EdSnakeState ev = (EdSnakeState) e;

		clearGraphics();

		grafTable[ev.getXPosition()][ev.getYPosition()].setIcon(Bouton.ROBOT);

		graf.getContentPane().repaint();
		
		try {
			Thread.sleep(50);
		} catch (InterruptedException e1) {
		}

	}

	@Override
	public void clearGraphics() {
		for (int i = 0; i < board.getWidthInTiles(); i++) {
			for (int j = 0; j < board.getHeightInTiles(); j++) {
				grafTable[i][j].setIcon(getCellType(i, j));
			}
		}
		graf.getContentPane().repaint();

	}

}
