package pl.edu.agh.iwium.edsnake;

public enum Move {
	UP(-1,0), DOWN(1,0), LEFT(0,-1), RIGHT(0,1);
	
	private int xMove;
	private int yMove;
	
	private Move(int y, int x) {
		xMove = x;
		yMove = y;
	}
	
	public int getXMove() {
		return xMove;
	}
	
	public int getYMove() {
		return yMove;
	}
}
