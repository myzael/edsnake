package pl.edu.agh.iwium.edsnake.learning.reinforcement;

import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Bouton extends JButton {

	private static final long serialVersionUID = 7215189888210012413L;

	protected static int UNKNOWN = 0, WALL = 1, TRESOR = 2, NEGATIVE_TRESOR = 3, ROBOT = 4;

	private static final String PATH = "/imgSmall/";

	private static ImageIcon tableau[] = {
		newImageFromResource("grass.jpg"), //
		newImageFromResource("rocks.jpg"), //
		newImageFromResource("res.png"), //
		newImageFromResource("res2.png"), //
		newImageFromResource("down1.png") };

	static {
		System.out.println("init");
	}

	private static ImageIcon newImageFromResource(String name) {
		URL resourcePath = Bouton.class.getResource(PATH + name);
		return new ImageIcon(resourcePath);
	}
	
	public Bouton(int i) {
		super(tableau[i]);
		setEnabled(false);
	}

	public void setIcon(int i) {

		this.setIcon(tableau[i]);
		this.setDisabledIcon(tableau[i]);
		this.repaint();
	}

}
