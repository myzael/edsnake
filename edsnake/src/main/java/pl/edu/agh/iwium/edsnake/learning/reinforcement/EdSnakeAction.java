package pl.edu.agh.iwium.edsnake.learning.reinforcement;

import pl.edu.agh.iwium.edsnake.Move;
import environment.IAction;

public class EdSnakeAction implements IAction {

	private static final long serialVersionUID = -579228253826206469L;
	private Move move;

	public EdSnakeAction(Move move) {
		this.move = move;
	}

	public Move getMove() {
		return move;
	}

	@Override
	public Object copy() {
		return new EdSnakeAction(move);
	}

	@Override
	public int nnCodingSize() {
		return 4;
	}

	@Override
	public double[] nnCoding() {
		double code[] = new double[4];
		code[move.ordinal()] = 1.0;
		return code;
	}

	@Override
	public int hashCode() {
		return move.ordinal();
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof EdSnakeAction))
			return false;
		EdSnakeAction action = (EdSnakeAction) o;
		return this.move == action.getMove();
	}

}
