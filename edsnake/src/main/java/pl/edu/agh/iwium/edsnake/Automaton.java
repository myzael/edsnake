package pl.edu.agh.iwium.edsnake;

public class Automaton extends AbstractPlayer implements IPlayer {
	private IStrategy strategy = new IStrategy() {
		@Override
		public Move getMove() {
			return Move.values()[(int) (Math.random() * 4)];
		}
	};

	@Override
	public IStrategy getStrategy() {
		return strategy;
	}

}
