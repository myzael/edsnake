package pl.edu.agh.iwium.edsnake.learning.reinforcement;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import pl.edu.agh.iwium.edsnake.GameInitializer;
import agents.IAgent;
import agents.LoneAgent;
import algorithms.QLearningSelector;

public class ReinforcementTest {

	private static PiqleBoard pBoard;

	private static EdSnakePlayer arbitre;

	private static QLearningSelector sql;

	public static void main(String[] argv) throws IOException {

		boolean[][] blocked = GameInitializer.readAndCreateBoard(20, 20);

		pBoard = new PiqleBoard(blocked);

		sql = new QLearningSelector();



		sql.setGeometricAlphaDecay(); // Ne marche pas pour Watkins

		// Build agent
		IAgent zero07 = new LoneAgent(pBoard, sql);

		arbitre = new EdSnakePlayer(zero07);
		arbitre.setMaxIter(1000);
		

		sql.setEpsilon(0.05);
		sql.setGamma(0.75);
		sql.setAlphaDecayPower(0.05); // A utiliser pour Watkins


		System.out.println("FINAL SCORE" + runSimulation(1001, 0.05, 0.9999));

	}

	private static double runSimulation(int episodes, double epsilon,
			double epsilonFactor) {

		double lastReward = 0;
		double reward = 0;
		int tailleEpisode = 20;

		for (int episode = 1; episode < episodes; episode++) {

			if (episode == 100)
				arbitre.setGraphical();
			pBoard.clearBoard();
			arbitre.episode(pBoard.defaultInitialState());
			reward += arbitre.getRewardForEpisode();
			if (((episode % tailleEpisode == 0) && (episode != 0))
					|| episode == 1) {
				System.out.println(episode + " "
						+ (reward / (0.0 + tailleEpisode)) + " " + epsilon);
				System.out.println("Episode : " + episode + " Average reward :"
						+ (reward / (0.0 + tailleEpisode)) + "POINTS: "
						+ pBoard.getPoints());
				lastReward = reward / (0.0 + tailleEpisode);
				reward = 0.0;
			}
			epsilon *= epsilonFactor;
			sql.setEpsilon(epsilon);
		}

		return lastReward;
	}
	
	private static void randomMutationHillClimbing(double step) throws IOException {
		FileWriter fw = new FileWriter(new File("C:\\Users\\magda\\Documents\\AGH\\IW\\log.txt"));
		
		fw.append("epsilon : gamma : alpha : result + \n");
		
		
		
		for (double epsilon = 0.0; epsilon <= 1.0; epsilon += step) {
			
			for (double gamma = 0.0; gamma <= 1.0; gamma += step) {
				sql.setGamma(gamma);

				for (double alpha = 0.0; alpha <= 1.0; alpha += step) {
					sql.setAlphaDecayPower(alpha);
					
					double result = 0.0;
					for (int i = 0; i < 10; i++) {
						sql.setEpsilon(epsilon);
						result += runSimulation(100, epsilon, 0.9999);
					}
					result = result / 10.0;
					System.out.println("Epsilon: " + epsilon + " Gamma: "
							+ gamma + " Alpha: " + alpha + " AVG SCORE: "
							+ result);
					fw.append(epsilon + " : " + gamma + " : " + alpha + " :: " + result + "\n");
					fw.flush();
					
					
				}
			}
		}
		
		
		fw.close();
	}

}
