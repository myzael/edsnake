package pl.edu.agh.iwium.edsnake;

public interface IStrategy {
	public Move getMove();
}
