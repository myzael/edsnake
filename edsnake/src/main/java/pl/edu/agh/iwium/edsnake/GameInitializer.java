package pl.edu.agh.iwium.edsnake;

public class GameInitializer {

	public static boolean[][] readAndCreateBoard(int width, int height) {

		boolean design[][] = new boolean[width][height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				design[i][j] = false;
			}
		}

		for (int i = ((width + 1) / 3); i < (2 * (width / 3)); i++)
			for (int j = 3; j < height - 2; j++)
				design[i][j] = true;
		// Vertical part of the cross
		for (int j = (height + 1) / 3; j < (2 * (height / 3)); j++)
			for (int i = 2; i < width - 2; i++)
				design[i][j] = true;

		return design;
	}
	
	public static boolean[][] readAndCreateEmptyBoard(int width, int height) {

		boolean design[][] = new boolean[width][height];

		return design;
	}

}
