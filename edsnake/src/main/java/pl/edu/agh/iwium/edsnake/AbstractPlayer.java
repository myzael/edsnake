package pl.edu.agh.iwium.edsnake;

import org.newdawn.slick.util.pathfinding.AStarPathFinder;

public abstract class AbstractPlayer implements IPlayer {

	private Board board;
	private int result = 0;

	@Override
	public void updateBoard(Board board) {
		this.board = board;
	}

	@Override
	public void awardPoints(int points) {
		result += points;
	}

	protected AStarPathFinder getPathFinder() {
		return board.getPathFinder();
	}

	@Override
	public Board getBoard() {
		return board;
	}

	@Override
	public int getResult() {
		return result;
	}

	@Override
	public void finishGame() {
	}
	
	@Override
	public void learn() {}
}
