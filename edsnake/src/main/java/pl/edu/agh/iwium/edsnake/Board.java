package pl.edu.agh.iwium.edsnake;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.newdawn.slick.util.pathfinding.AStarPathFinder;
import org.newdawn.slick.util.pathfinding.PathFindingContext;
import org.newdawn.slick.util.pathfinding.TileBasedMap;

public class Board implements TileBasedMap {

	private static final float DEF_RES_GEN_PROB = 0.3f;
	private static final int DEF_RES_VAL_LO = -10;
	private static final int DEF_RES_VAL_UP = 10;
	private static final int DEF_RES_MAX_LIFE = 50;
	private static final int DEF_INIT_RESOURCES = 10;
	private final boolean[][] map;
	private final int width;
	private final int height;
	private final Map<Position, Resource> resourcesMap;
	private int x, y;

	private final float resourceGenProb;
	private final int resourceValLowerBound;
	private final int resourceValUpperBound;
	private final int resourceMaxLife;
	private final int initialResourcesCount;
	private AStarPathFinder pathFinder;
	private int lastReward;
	private final List<ResourcesListener> resourcesListeners = new LinkedList<>();

	public Board(boolean[][] blocked) {
		this(blocked, DEF_RES_GEN_PROB, DEF_RES_VAL_LO, DEF_RES_VAL_UP, DEF_RES_MAX_LIFE, DEF_INIT_RESOURCES);
	}

	public Board(boolean[][] blocked, float resourceGenProb, int resourceValLowerBound, int resourceValUpperBound, int resourceMaxLife, int initialResourcesCount) {
		assert blocked.length > 0;
		this.map = blocked;
		this.width = blocked.length;
		this.height = blocked[0].length;
		this.resourcesMap = new HashMap<>();
		this.resourceGenProb = resourceGenProb;
		this.resourceValLowerBound = resourceValLowerBound;
		this.resourceValUpperBound = resourceValUpperBound;
		this.resourceMaxLife = resourceMaxLife;
		this.initialResourcesCount = initialResourcesCount;
		this.lastReward = 0;

		generateInitialResources();

		createPathFinder();
	}

	public void register(ResourcesListener resourcesListener) {
		resourcesListeners.add(resourcesListener);
	}

	private void generateInitialResources() {
		generatePositiveResource();
		if (initialResourcesCount > 1) {
			for (int i = 0; i < initialResourcesCount - 1; i++) {
				generateResource();
			}
		}
	}

	@Override
	public boolean blocked(PathFindingContext context, int x, int y) {
		if (x >= width || y >= height || x < 0 || y < 0) {
			return true;
		}
		return map[x][y];
	}

	public boolean blocked(int x, int y) {
		return blocked(null, x, y);
	}

	@Override
	public float getCost(PathFindingContext arg0, int arg1, int arg2) {
		return 1.0f;
	}

	@Override
	public int getHeightInTiles() {
		return height;
	}

	@Override
	public int getWidthInTiles() {
		return width;
	}

	@Override
	public void pathFinderVisited(int arg0, int arg1) {
	}

	public boolean isResource(int x, int y) {
		return resourcesMap.containsKey(new Position(x, y));
	}

	public void updateBoard() {
		List<Position> positionsToRemove = new ArrayList<>();
		for (Position p : resourcesMap.keySet()) {
			if (resourcesMap.get(p).getLife() <= 1) {
				positionsToRemove.add(p);
			} else {
				resourcesMap.get(p).makeOlder();
			}
		}
		for (Position position : positionsToRemove) {
			Resource expiredResource = resourcesMap.remove(position);
			resourceExpiredNotifyAll(expiredResource);
		}

		if (Math.random() < resourceGenProb) {
			generateResource();
		}
		if (noPositiveResourcePresent()) {
			generatePositiveResource();
		}
	}

	public int consumeResource(int x, int y) {
		Position position = new Position(x, y);
		Resource eatenResource = resourcesMap.remove(position);
		resourceEatenNotifyAll(eatenResource);

		if (noPositiveResourcePresent()) {
			generatePositiveResource();
		}
		return eatenResource.getValue();
	}

	private boolean noPositiveResourcePresent() {
		for (Position p : resourcesMap.keySet()) {
			if (resourcesMap.get(p).getValue() > 0) {
				return false;
			}
		}
		return true;
	}

	private void createPathFinder() {
		int maxPathLength = getHeightInTiles() * getWidthInTiles() * 10;// arbitrary
		pathFinder = new AStarPathFinder(this, maxPathLength, false);
	}

	public AStarPathFinder getPathFinder() {
		return pathFinder;
	}

	public Map<Position, Resource> getResourcesMap() {
		return Collections.unmodifiableMap(resourcesMap);
	}

	private Position createRandomAvaliablePosition() {
		int x, y;
		do {
			x = (int) (Math.random() * width);
			y = (int) (Math.random() * height);
		} while (blocked(x, y));
		return new Position(x, y);
	}

	private void generateResource() {
		Position avilablePosition = createRandomAvaliablePosition();
		addResourceToBoard(avilablePosition, createResource(avilablePosition));
	}

	private void generatePositiveResource() {
		Position avilablePosition = createRandomAvaliablePosition();
		addResourceToBoard(avilablePosition, createPositiveResource(avilablePosition));
	}

	private void addResourceToBoard(Position position, Resource resource) {
		resourcesMap.put(position, resource);
		resourceGeneratedNotifyAll(resource);
	}

	private Resource createPositiveResource(Position position) {
		int value = (int) (Math.random() * (resourceValUpperBound));
		int life = (int) (Math.random() * resourceMaxLife);
		return new Resource(position, value, life);
	}

	private Resource createResource(Position position) {
		int value = (int) (Math.random() * (resourceValUpperBound - resourceValLowerBound) + resourceValLowerBound);
		int life = (int) (Math.random() * resourceMaxLife);
		return new Resource(position, value, life);
	}

	public int getLastReward() {
		return lastReward;
	}

	public void setLastReward(int lastReward) {
		this.lastReward = lastReward;
	}

	public void setPlayerPosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void setPlayerPosition(Position p) {
		this.x = p.getX();
		this.y = p.getY();
	}

	public Position getPlayerPostition() {
		return new Position(x, y);
	}
	
	private void resourceGeneratedNotifyAll(Resource resource) {
		for (ResourcesListener resourcesListener : resourcesListeners) {
			resourcesListener.resourceGenerated(resource);
		}
	}
	
	private void resourceExpiredNotifyAll(Resource resource) {
		for (ResourcesListener resourcesListener : resourcesListeners) {
			resourcesListener.resourceExpired(resource);
		}
	}
	
	private void resourceEatenNotifyAll(Resource resource) {
		for (ResourcesListener resourcesListener : resourcesListeners) {
			resourcesListener.resourceEaten(resource);
		}
	}
}
