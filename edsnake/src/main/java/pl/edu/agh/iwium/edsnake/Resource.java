package pl.edu.agh.iwium.edsnake;

import com.google.common.base.Objects;

public class Resource {
	private final Position position;
	private final int value;
	private int life;
	
	public Resource(Position position, int value, int life) {
		this.position = position;
		this.value = value;
		this.life = life;
	}
	
	public void makeOlder() {
		life--;
	}

	public int getValue() {
		return value;
	}

	public int getLife() {
		return life;
	}

	public Position getPosition() {
		return position;
	}
	
	@Override
	public String toString() {
		return Objects.toStringHelper(this). //
				add("x", position.getX()). //
				add("y", position.getY()). //
				add("value", value). //
				add("life", life). //
				toString();
	}
}
