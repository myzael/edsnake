package pl.edu.agh.iwium.edsnake.supervised.astar.player

import scala.language.postfixOps
import scala.Option.option2Iterable
import scala.collection.JavaConversions.mapAsScalaMap

import com.google.common.base.Objects
import com.typesafe.scalalogging.slf4j.Logging

import pl.edu.agh.iwium.edsnake.AbstractPlayer
import pl.edu.agh.iwium.edsnake.Game
import pl.edu.agh.iwium.edsnake.IStrategy
import pl.edu.agh.iwium.edsnake.Move
import pl.edu.agh.iwium.edsnake.Move.DOWN
import pl.edu.agh.iwium.edsnake.Move.LEFT
import pl.edu.agh.iwium.edsnake.Move.RIGHT
import pl.edu.agh.iwium.edsnake.Move.UP
import pl.edu.agh.iwium.edsnake.Position
import pl.edu.agh.iwium.edsnake.Resource
import pl.edu.agh.iwium.edsnake.ResourcesListener
import pl.edu.agh.iwium.edsnake.astar.AStartPathFinder
import pl.edu.agh.iwium.edsnake.supervised.astar.AStarModel

case class ResourceInfo(resource: Resource, distribution: List[Double], distance: Int, moves: Iterator[Move]) {

  override def toString(): String = {
    val pos = resource.getPosition()
    return Objects.toStringHelper(this).
      add("resource", resource).
      add("distance", distance).
      add("distribution", distribution.mkString("[", ", ", "]")).
      toString()
  }
}

object ResourceInfo extends Logging {

  def apply(): ResourceInfo = {
    val resource = new Resource(new Position(0, 0), 0, 0)
    return ResourceInfo(resource, List(0.0, 0.0, 0.0, -1.0), 0, Iterator.empty)
  }

  def betterOrLeft(resourceA: ResourceInfo, resourceB: ResourceInfo): ResourceInfo = {
    val zipped = resourceA.distribution zip resourceB.distribution

    for ((a, b) <- zipped) {
      if (a >= b) {
        return resourceA
      } else {
        logger.debug(s"new best changed $resourceB ($resourceA)")
        return resourceB
      }
    }

    return resourceA
  }
}

class ResourceInfoBuilder(model: AStarModel) {

  def apply(resource: Resource, pathToResource: List[Move]): ResourceInfo = {
    val distance = pathToResource.size
    val value = resource.getValue
    val life = resource.getLife

    val testCase = model.builder.distance(distance).value(value).life(life).buildTest
    val distribution = model.distribution(testCase)

    return ResourceInfo(resource, distribution, pathToResource.size, pathToResource.iterator)
  }
}

class AStarSupervisedLearingPlayer extends AbstractPlayer {

  override def getStrategy(): IStrategy = {
    return strategy
  }

  lazy val strategy = new IStrategy with Logging with ResourcesListener {

    private val board = getBoard()

    private val pathFinder = new AStartPathFinder(board)

    private val model = new AStarModel()

    private val resourceInfoBuilder = new ResourceInfoBuilder(model)

    private var bestResource: ResourceInfo = ResourceInfo()

    board.register(this)

    logger.debug("\n{}", model)

    override def getMove(): Move = {
      if (bestResource == null) {
        bestResource = findBestResource()
      }

      if (!bestResource.moves.hasNext) {
        bestResource = findBestResource()
        logger.debug(s"new best: $bestResource")
      }

      if (!bestResource.moves.hasNext) {
        logger.debug("no valid moves")
        return getValidMove()
      }

      return bestResource.moves.next
    }

    private def getValidMove(): Move = {
      val x = board.getPlayerPostition.getX
      val y = board.getPlayerPostition.getY

      return if (!board.blocked(x + 1, y)) {
        RIGHT
      } else if (!board.blocked(x - 1, y)) {
        LEFT
      } else if (!board.blocked(x, y + 1)) {
        UP
      } else {
        DOWN
      }
    }

    private def findBestResource(): ResourceInfo = {
      val resources = getBoard().getResourcesMap()

      val resourcesInfo = resources flatMap {
        case (resourcePositon, resource) => {
          val path = pathFinder.findPath(resourcePositon)

          path match {
            case Some(path) => Some(resourceInfoBuilder(resource, path))
            case None => None
          }
        }
      } toList

      if (resourcesInfo.isEmpty) {
        return ResourceInfo()
      }

      val bestResource = resourcesInfo.view.
        sortBy(-_.distribution(3)).
        sortBy(-_.distribution(2)).
        sortBy(-_.distribution(1)).
        sortBy(-_.distribution(0)).head

      return bestResource
    }

    override def resourceGenerated(resource: Resource) {
      val path = pathFinder.findPath(resource.getPosition)

      path match {
        case Some(path) => {
          bestResource = reevaluateBest()
          val candidate = resourceInfoBuilder(resource, path)
          bestResource = ResourceInfo.betterOrLeft(bestResource, candidate)
        }
        case None =>
      }
    }

    private def reevaluateBest(): ResourceInfo = {
      val newPath = pathFinder.findPath(bestResource.resource.getPosition())

      return newPath match {
        case Some(newPath) => return resourceInfoBuilder(bestResource.resource, newPath)
        case None => bestResource
      }
    }

    override def resourceExpired(resource: Resource) {
      if (bestResource.resource.getPosition() == resource.getPosition()) {
        logger.debug("Search expired {}", resource)
        bestResource = findBestResource()
      }
    }

    override def resourceEaten(resource: Resource) {
      logger.debug("Eaten {}", resource)
    }
  }
}

object AStarSupervisedLearingPlayer extends App {
  Game.run(new AStarSupervisedLearingPlayer(), 1000, 0)
}