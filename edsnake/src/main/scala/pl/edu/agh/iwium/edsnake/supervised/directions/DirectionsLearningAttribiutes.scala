package pl.edu.agh.iwium.edsnake.supervised.directions

import scala.collection.mutable.LinkedHashMap

import pl.edu.agh.iwium.edsnake.Move._

import weka.classifiers.Classifier
import weka.core.Attribute
import weka.core.FastVector
import weka.core.Instances

class DirectionsLearningAttribiutes {

  val attributes = createAttibiutes()
  val trainingSet = createTrainingSet()

  private def createAttibiutes(): LinkedHashMap[String, Attribute] = {
    val attributes = new LinkedHashMap[String, Attribute]
    def addAttribiute(attribute: Attribute) {
      attributes += attribute.name -> attribute
    }

//    addAttribiute(new Attribute("x"))
//    addAttribiute(new Attribute("y"))
//    addAttribiute(new Attribute("life"))
    addAttribiute(new Attribute("right"))
    addAttribiute(new Attribute("down"))
    addAttribiute(new Attribute("value"))

    addAttribiute(createMoveAttribute("move1"))
    addAttribiute(createMoveAttribute("move2"))
    addAttribiute(createMoveAttribute("move3"))
    addAttribiute(createMoveAttribute("move4"))
    
    val clazzAttribute = new FastVector(2);
    clazzAttribute.addElement("+");
    clazzAttribute.addElement("-");
    addAttribiute(new Attribute("clazz", clazzAttribute))

    return attributes
  }

  private def createMoveAttribute(name: String): Attribute = {
    val moveAttribiute = new FastVector(4);
    moveAttribiute.addElement(LEFT.toString);
    moveAttribiute.addElement(RIGHT.toString);
    moveAttribiute.addElement(UP.toString);
    moveAttribiute.addElement(DOWN.toString);

    return new Attribute(name, moveAttribiute)
  }
  
  private def createTrainingSet(): Instances = {
    val attributesVector = new FastVector(attributes.size)
    attributes.values.foreach { attr => attributesVector.addElement(attr) }
    val trainingSet = new Instances("snake", attributesVector, 100)

    return trainingSet
  }

  def apply(name: String): Attribute = {
    return attributes(name)
  }

  def addExampleToTrainingSet(snakeExample: DirectionsExample) {
    trainingSet.add(snakeExample)
  }

  def addExampleToTrainingSet(snakeExamples: List[DirectionsExample]) {
    snakeExamples.foreach(addExampleToTrainingSet)
  }

  def buildModel(model: Classifier, clazz: String): Classifier = {
    trainingSet.setClass(attributes(clazz))
    model.buildClassifier(trainingSet)
    return model
  }
}