package pl.edu.agh.iwium.edsnake.astar.player

import scala.util.Random
import pl.edu.agh.iwium.edsnake.astar.AStartPathFinder
import pl.edu.agh.iwium.edsnake.AbstractPlayer
import pl.edu.agh.iwium.edsnake.Game
import pl.edu.agh.iwium.edsnake.IStrategy
import pl.edu.agh.iwium.edsnake.Move
import collection.JavaConversions._

class AStartRandomPlayer extends AbstractPlayer {

  override def getStrategy(): IStrategy = {
    return strategy
  }

  lazy val strategy = new IStrategy {

    private val board = getBoard()

    private val pathFinder = new AStartPathFinder(board)

    private var moves = getRandomResource

    //board.register(new ExampleGenerator(pathFinder))

    override def getMove(): Move = {
      if (!moves.hasNext) {
        moves = getRandomResource()
      }
      return moves.next
    }

    private def getRandomResource(): Iterator[Move] = {
      return getRandomResource(0)
    }

    private def getRandomResource(tries: Int): Iterator[Move] = {
      val resources = getBoard().getResourcesMap()
      val randomResourcePosition = resources.keySet().toList(Random.nextInt(resources.size()))

      return pathFinder.findPath(randomResourcePosition) match {
        case Some(path) => path.iterator
        case None if tries < 10 => getRandomResource(tries + 1)
        case None => List(Move.UP).iterator
      }
    }
  }
}

object AStartRandomPlayer extends App {
  Game.run(new AStartRandomPlayer(), 1000, 0)
}