package pl.edu.agh.iwium.edsnake.supervised.astar

import scala.collection.mutable.LinkedHashMap

import pl.edu.agh.iwium.edsnake.supervised.astar.AStarExample.snakeExampleToWekaInstance
import weka.classifiers.Classifier
import weka.core.Attribute
import weka.core.FastVector
import weka.core.Instances

class AStarLearningAttribiutes {

  val attributes = createAttibiutes()
  val trainingSet = createTrainingSet()

  private def createAttibiutes(): LinkedHashMap[String, Attribute] = {
    val attributes = new LinkedHashMap[String, Attribute]
    def addAttribiute(attribute: Attribute) {
      attributes += attribute.name -> attribute
    }

    addAttribiute(new Attribute("distance"))
    addAttribiute(new Attribute("value"))
    addAttribiute(new Attribute("life"))

    val clazzAttribute = new FastVector(2);
    clazzAttribute.addElement("0");
    clazzAttribute.addElement("1");
    clazzAttribute.addElement("2");
    clazzAttribute.addElement("-1");
    addAttribiute(new Attribute("clazz", clazzAttribute))

    return attributes
  }

  private def createTrainingSet(): Instances = {
    val attributesVector = new FastVector(attributes.size)
    attributes.values.foreach { attr => attributesVector.addElement(attr) }
    val trainingSet = new Instances("snake", attributesVector, 100)
    trainingSet.setClass(attributes("clazz"))

    return trainingSet
  }

  def apply(name: String): Attribute = {
    return attributes(name)
  }

  def addExampleToTrainingSet(snakeExample: AStarExample) {
    trainingSet.add(snakeExample)
  }

  def addExampleToTrainingSet(snakeExamples: List[AStarExample]) {
    snakeExamples.foreach(addExampleToTrainingSet)
  }

  def buildModel(model: Classifier): Classifier = {
    model.buildClassifier(trainingSet)
    return model
  }
}