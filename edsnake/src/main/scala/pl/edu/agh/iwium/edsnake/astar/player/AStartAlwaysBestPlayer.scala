package pl.edu.agh.iwium.edsnake.astar.player

import scala.util.Random
import pl.edu.agh.iwium.edsnake.astar.AStartPathFinder
import pl.edu.agh.iwium.edsnake.AbstractPlayer
import pl.edu.agh.iwium.edsnake.Game
import pl.edu.agh.iwium.edsnake.IStrategy
import pl.edu.agh.iwium.edsnake.Move
import collection.JavaConversions._
import pl.edu.agh.iwium.edsnake.Position
import pl.edu.agh.iwium.edsnake.ResourcesListener
import pl.edu.agh.iwium.edsnake.Resource

class AStartAlwaysBestPlayer extends AbstractPlayer {

  override def getStrategy(): IStrategy = {
    return strategy
  }

  lazy val strategy = new IStrategy with ResourcesListener {

    private val board = getBoard()

    private val pathFinder = new AStartPathFinder(board)

    private var target: Option[Position] = None

    private var moves = getBestResource

    board.register(this)

    override def getMove(): Move = {
      if (!moves.hasNext) {
        moves = getBestResource()
      }
      return moves.next
    }

    private def getBestResource(): Iterator[Move] = {
      val resources = getBoard().getResourcesMap()
      val best = resources.values().maxBy(_.getValue())

      return pathFinder.findPath(best.getPosition()) match {
        case Some(path) => {
          target = Some(best.getPosition())
          path.iterator
        }
        case None => {
          target = None
          Iterator(Move.UP)
        }
      }
    }

    override def resourceGenerated(resource: Resource) {

    }

    override def resourceExpired(resource: Resource) {
      deleteIfTarget(resource)
    }

    override def resourceEaten(resource: Resource) {
      deleteIfTarget(resource)
    }
    
    private def deleteIfTarget(resource: Resource) {
      target = target match {
        case Some(target) if target == resource.getPosition() => {
          moves = Iterator.empty
          None
        }
        case _ => target
      }
    }
  }
}

object AStartAlwaysBestPlayer extends App {
  Game.run(new AStartAlwaysBestPlayer(), 1000, 0)
}