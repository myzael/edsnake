package pl.edu.agh.iwium.edsnake.supervised.directions

import weka.classifiers.trees.J48
import pl.edu.agh.iwium.edsnake.Move
import pl.edu.agh.iwium.edsnake.Move._

case class DistributionWithMove(distribution: Double, move: Move)

object DistributionWithMove {
  implicit def toMove(distributionWithMove: DistributionWithMove): Move = distributionWithMove.move
}

object DirectionsModel {
  private val attr = new DirectionsLearningAttribiutes()

  builder.down(4).right(0).value(3).move1(DOWN).move2(DOWN).move3(DOWN).move4(DOWN).clazz("+").buildTraining
  builder.down(-3).right(1).value(4).move1(UP).move2(UP).move3(UP).move4(RIGHT).clazz("+").buildTraining
  builder.down(2).right(2).value(5).move1(DOWN).move2(DOWN).move3(RIGHT).move4(RIGHT).clazz("+").buildTraining
  builder.down(-3).right(-1).value(6).move1(UP).move2(UP).move3(UP).move4(LEFT).clazz("+").buildTraining

  def builder() = DirectionsExample.builder(attr)
}

class DirectionsModel(clazz: String) {

  private val classifier = new J48()

  private val model = DirectionsModel.attr.buildModel(classifier, clazz)

  def distribution(testExample: DirectionsExample): List[Double] = {
    return model.distributionForInstance(testExample).toList
  }

  def moveDistribution(testExample: DirectionsExample): List[DistributionWithMove] = {
    val dist = distribution(testExample)
    val distributionWithMove = dist zip List(LEFT, RIGHT, UP, DOWN)

    return distributionWithMove map { e => DistributionWithMove(e._1, e._2) }
  }

  override def toString(): String = classifier.toString()
}