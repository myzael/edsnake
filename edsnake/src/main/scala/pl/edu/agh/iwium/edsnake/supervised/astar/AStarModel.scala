package pl.edu.agh.iwium.edsnake.supervised.astar

import pl.edu.agh.iwium.edsnake.supervised.astar.AStarExample.snakeExampleToWekaInstance
import weka.classifiers.trees.J48

class AStarModel {

  private val attr = new AStarLearningAttribiutes()

  AStarExample.builder(attr).life(0).distance(12).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(5).distance(20).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(8).distance(13).value(2).clazz("2").buildTraining
  AStarExample.builder(attr).life(3).distance(15).value(-2).clazz("-1").buildTraining
  AStarExample.builder(attr).life(13).distance(7).value(2).clazz("1").buildTraining
  AStarExample.builder(attr).life(18).distance(12).value(9).clazz("1").buildTraining
  AStarExample.builder(attr).life(12).distance(17).value(8).clazz("2").buildTraining
  AStarExample.builder(attr).life(0).distance(12).value(6).clazz("2").buildTraining
  AStarExample.builder(attr).life(12).distance(16).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(9).distance(26).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(11).distance(9).value(4).clazz("1").buildTraining
  AStarExample.builder(attr).life(16).distance(8).value(-2).clazz("-1").buildTraining
  AStarExample.builder(attr).life(10).distance(13).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(12).distance(13).value(6).clazz("2").buildTraining
  AStarExample.builder(attr).life(19).distance(6).value(1).clazz("1").buildTraining
  AStarExample.builder(attr).life(15).distance(3).value(8).clazz("0").buildTraining
  AStarExample.builder(attr).life(16).distance(15).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(12).distance(2).value(4).clazz("0").buildTraining
  AStarExample.builder(attr).life(14).distance(2).value(3).clazz("0").buildTraining
  AStarExample.builder(attr).life(6).distance(4).value(4).clazz("1").buildTraining
  AStarExample.builder(attr).life(11).distance(10).value(3).clazz("1").buildTraining
  AStarExample.builder(attr).life(16).distance(19).value(1).clazz("2").buildTraining
  AStarExample.builder(attr).life(8).distance(12).value(-1).clazz("-1").buildTraining
  AStarExample.builder(attr).life(12).distance(18).value(-2).clazz("-1").buildTraining
  AStarExample.builder(attr).life(1).distance(6).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(0).distance(7).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(4).distance(17).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(19).distance(6).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(10).distance(7).value(2).clazz("1").buildTraining
  AStarExample.builder(attr).life(10).distance(4).value(7).clazz("0").buildTraining
  AStarExample.builder(attr).life(2).distance(7).value(7).clazz("2").buildTraining
  AStarExample.builder(attr).life(4).distance(13).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(4).distance(12).value(2).clazz("2").buildTraining
  AStarExample.builder(attr).life(9).distance(2).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(2).distance(28).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(8).distance(28).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(16).distance(29).value(6).clazz("2").buildTraining
  AStarExample.builder(attr).life(15).distance(5).value(3).clazz("1").buildTraining
  AStarExample.builder(attr).life(5).distance(21).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(12).distance(13).value(5).clazz("2").buildTraining
  AStarExample.builder(attr).life(16).distance(13).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(14).distance(3).value(1).clazz("1").buildTraining
  AStarExample.builder(attr).life(3).distance(5).value(1).clazz("2").buildTraining
  AStarExample.builder(attr).life(10).distance(6).value(9).clazz("0").buildTraining
  AStarExample.builder(attr).life(4).distance(9).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(15).distance(9).value(1).clazz("1").buildTraining
  AStarExample.builder(attr).life(0).distance(6).value(8).clazz("2").buildTraining
  AStarExample.builder(attr).life(19).distance(25).value(1).clazz("2").buildTraining
  AStarExample.builder(attr).life(10).distance(13).value(-1).clazz("-1").buildTraining
  AStarExample.builder(attr).life(1).distance(4).value(9).clazz("2").buildTraining
  AStarExample.builder(attr).life(10).distance(9).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(2).distance(8).value(8).clazz("2").buildTraining
  AStarExample.builder(attr).life(16).distance(16).value(-2).clazz("-1").buildTraining
  AStarExample.builder(attr).life(2).distance(6).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(19).distance(18).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(2).distance(20).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(2).distance(19).value(1).clazz("2").buildTraining
  AStarExample.builder(attr).life(3).distance(17).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(6).distance(24).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(12).distance(6).value(2).clazz("1").buildTraining
  AStarExample.builder(attr).life(9).distance(16).value(1).clazz("2").buildTraining
  AStarExample.builder(attr).life(15).distance(19).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(3).distance(11).value(-1).clazz("-1").buildTraining
  AStarExample.builder(attr).life(9).distance(9).value(1).clazz("1").buildTraining
  AStarExample.builder(attr).life(4).distance(8).value(-2).clazz("-1").buildTraining
  AStarExample.builder(attr).life(10).distance(4).value(5).clazz("0").buildTraining
  AStarExample.builder(attr).life(1).distance(4).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(15).distance(2).value(9).clazz("0").buildTraining
  AStarExample.builder(attr).life(1).distance(18).value(1).clazz("2").buildTraining
  AStarExample.builder(attr).life(15).distance(21).value(7).clazz("2").buildTraining
  AStarExample.builder(attr).life(4).distance(7).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(11).distance(9).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(2).distance(15).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(7).distance(10).value(1).clazz("2").buildTraining
  AStarExample.builder(attr).life(9).distance(13).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(1).distance(3).value(6).clazz("2").buildTraining
  AStarExample.builder(attr).life(9).distance(16).value(6).clazz("2").buildTraining
  AStarExample.builder(attr).life(8).distance(14).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(8).distance(6).value(9).clazz("0").buildTraining
  AStarExample.builder(attr).life(18).distance(16).value(8).clazz("1").buildTraining
  AStarExample.builder(attr).life(7).distance(14).value(5).clazz("2").buildTraining
  AStarExample.builder(attr).life(16).distance(15).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(1).distance(24).value(-1).clazz("-1").buildTraining
  AStarExample.builder(attr).life(16).distance(6).value(7).clazz("1").buildTraining
  AStarExample.builder(attr).life(7).distance(16).value(-2).clazz("-1").buildTraining
  AStarExample.builder(attr).life(10).distance(14).value(2).clazz("2").buildTraining
  AStarExample.builder(attr).life(6).distance(10).value(7).clazz("2").buildTraining
  AStarExample.builder(attr).life(6).distance(13).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(15).distance(5).value(1).clazz("1").buildTraining
  AStarExample.builder(attr).life(16).distance(15).value(6).clazz("1").buildTraining
  AStarExample.builder(attr).life(5).distance(10).value(2).clazz("2").buildTraining
  AStarExample.builder(attr).life(13).distance(16).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(8).distance(6).value(3).clazz("1").buildTraining
  AStarExample.builder(attr).life(13).distance(7).value(4).clazz("1").buildTraining
  AStarExample.builder(attr).life(1).distance(15).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(4).distance(6).value(5).clazz("2").buildTraining
  AStarExample.builder(attr).life(2).distance(6).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(10).distance(8).value(4).clazz("1").buildTraining
  AStarExample.builder(attr).life(9).distance(25).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(17).distance(12).value(4).clazz("1").buildTraining
  AStarExample.builder(attr).life(14).distance(17).value(1).clazz("2").buildTraining
  AStarExample.builder(attr).life(1).distance(5).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(0).distance(4).value(7).clazz("2").buildTraining
  AStarExample.builder(attr).life(2).distance(1).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(3).distance(18).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(2).distance(21).value(6).clazz("2").buildTraining
  AStarExample.builder(attr).life(12).distance(9).value(8).clazz("1").buildTraining
  AStarExample.builder(attr).life(11).distance(1).value(4).clazz("0").buildTraining
  AStarExample.builder(attr).life(7).distance(11).value(5).clazz("2").buildTraining
  AStarExample.builder(attr).life(7).distance(11).value(9).clazz("2").buildTraining
  AStarExample.builder(attr).life(4).distance(7).value(5).clazz("2").buildTraining
  AStarExample.builder(attr).life(1).distance(12).value(8).clazz("2").buildTraining
  AStarExample.builder(attr).life(13).distance(8).value(5).clazz("1").buildTraining
  AStarExample.builder(attr).life(2).distance(11).value(9).clazz("2").buildTraining
  AStarExample.builder(attr).life(0).distance(22).value(1).clazz("2").buildTraining
  AStarExample.builder(attr).life(7).distance(10).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(7).distance(10).value(5).clazz("2").buildTraining
  AStarExample.builder(attr).life(4).distance(10).value(-2).clazz("-1").buildTraining
  AStarExample.builder(attr).life(2).distance(8).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(4).distance(18).value(6).clazz("2").buildTraining
  AStarExample.builder(attr).life(1).distance(4).value(1).clazz("2").buildTraining
  AStarExample.builder(attr).life(4).distance(12).value(8).clazz("2").buildTraining
  AStarExample.builder(attr).life(12).distance(2).value(-1).clazz("-1").buildTraining
  AStarExample.builder(attr).life(14).distance(9).value(7).clazz("1").buildTraining
  AStarExample.builder(attr).life(11).distance(20).value(8).clazz("2").buildTraining
  AStarExample.builder(attr).life(19).distance(1).value(5).clazz("0").buildTraining
  AStarExample.builder(attr).life(6).distance(17).value(1).clazz("2").buildTraining
  AStarExample.builder(attr).life(5).distance(3).value(1).clazz("1").buildTraining
  AStarExample.builder(attr).life(6).distance(14).value(9).clazz("2").buildTraining
  AStarExample.builder(attr).life(17).distance(10).value(7).clazz("1").buildTraining
  AStarExample.builder(attr).life(12).distance(4).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(19).distance(16).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(16).distance(1).value(7).clazz("0").buildTraining
  AStarExample.builder(attr).life(16).distance(16).value(-1).clazz("-1").buildTraining
  AStarExample.builder(attr).life(2).distance(12).value(5).clazz("2").buildTraining
  AStarExample.builder(attr).life(2).distance(22).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(15).distance(17).value(6).clazz("2").buildTraining
  AStarExample.builder(attr).life(5).distance(15).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(12).distance(11).value(7).clazz("1").buildTraining
  AStarExample.builder(attr).life(3).distance(15).value(5).clazz("2").buildTraining
  AStarExample.builder(attr).life(3).distance(5).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(1).distance(9).value(2).clazz("2").buildTraining
  AStarExample.builder(attr).life(1).distance(10).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(6).distance(27).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(16).distance(18).value(2).clazz("2").buildTraining
  AStarExample.builder(attr).life(19).distance(11).value(7).clazz("1").buildTraining
  AStarExample.builder(attr).life(19).distance(9).value(4).clazz("1").buildTraining
  AStarExample.builder(attr).life(13).distance(10).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(0).distance(7).value(5).clazz("2").buildTraining
  AStarExample.builder(attr).life(5).distance(17).value(1).clazz("2").buildTraining
  AStarExample.builder(attr).life(12).distance(8).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(18).distance(17).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(6).distance(9).value(2).clazz("2").buildTraining
  AStarExample.builder(attr).life(3).distance(13).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(14).distance(17).value(8).clazz("2").buildTraining
  AStarExample.builder(attr).life(17).distance(15).value(2).clazz("2").buildTraining
  AStarExample.builder(attr).life(11).distance(9).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(1).distance(17).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(19).distance(16).value(9).clazz("1").buildTraining
  AStarExample.builder(attr).life(15).distance(18).value(6).clazz("2").buildTraining
  AStarExample.builder(attr).life(4).distance(20).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(2).distance(11).value(1).clazz("2").buildTraining
  AStarExample.builder(attr).life(14).distance(18).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(15).distance(7).value(8).clazz("2").buildTraining
  AStarExample.builder(attr).life(11).distance(7).value(-1).clazz("-1").buildTraining
  AStarExample.builder(attr).life(15).distance(2).value(8).clazz("0").buildTraining
  AStarExample.builder(attr).life(6).distance(14).value(3).clazz("2").buildTraining
  AStarExample.builder(attr).life(4).distance(12).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(9).distance(18).value(8).clazz("2").buildTraining
  AStarExample.builder(attr).life(14).distance(10).value(3).clazz("1").buildTraining
  AStarExample.builder(attr).life(5).distance(11).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(2).distance(2).value(9).clazz("0").buildTraining
  AStarExample.builder(attr).life(11).distance(12).value(5).clazz("2").buildTraining
  AStarExample.builder(attr).life(6).distance(7).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(15).distance(14).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(11).distance(16).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(15).distance(15).value(2).clazz("2").buildTraining
  AStarExample.builder(attr).life(8).distance(15).value(2).clazz("2").buildTraining
  AStarExample.builder(attr).life(1).distance(22).value(5).clazz("2").buildTraining
  AStarExample.builder(attr).life(6).distance(17).value(7).clazz("2").buildTraining
  AStarExample.builder(attr).life(2).distance(1).value(3).clazz("0").buildTraining
  AStarExample.builder(attr).life(4).distance(27).value(6).clazz("2").buildTraining
  AStarExample.builder(attr).life(7).distance(8).value(9).clazz("2").buildTraining
  AStarExample.builder(attr).life(15).distance(4).value(8).clazz("0").buildTraining
  AStarExample.builder(attr).life(17).distance(6).value(9).clazz("0").buildTraining
  AStarExample.builder(attr).life(19).distance(1).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(0).distance(6).value(2).clazz("2").buildTraining
  AStarExample.builder(attr).life(7).distance(3).value(2).clazz("0").buildTraining
  AStarExample.builder(attr).life(10).distance(16).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(3).distance(11).value(9).clazz("2").buildTraining
  AStarExample.builder(attr).life(11).distance(5).value(9).clazz("0").buildTraining
  AStarExample.builder(attr).life(6).distance(3).value(7).clazz("0").buildTraining
  AStarExample.builder(attr).life(17).distance(13).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(0).distance(11).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(1).distance(8).value(5).clazz("2").buildTraining
  AStarExample.builder(attr).life(1).distance(1).value(5).clazz("0").buildTraining
  AStarExample.builder(attr).life(9).distance(8).value(8).clazz("1").buildTraining
  AStarExample.builder(attr).life(9).distance(5).value(5).clazz("0").buildTraining
  AStarExample.builder(attr).life(8).distance(12).value(4).clazz("2").buildTraining
  AStarExample.builder(attr).life(5).distance(10).value(-2).clazz("-1").buildTraining
  AStarExample.builder(attr).life(17).distance(22).value(7).clazz("2").buildTraining
  AStarExample.builder(attr).life(9).distance(11).value(2).clazz("2").buildTraining
  AStarExample.builder(attr).life(19).distance(4).value(0).clazz("2").buildTraining
  AStarExample.builder(attr).life(8).distance(14).value(8).clazz("2").buildTraining
  AStarExample.builder(attr).life(11).distance(17).value(1).clazz("2").buildTraining

  private val classifier = new J48()

  private val model = attr.buildModel(classifier)

  def builder() = AStarExample.builder(attr)

  def distribution(testExample: AStarExample): List[Double] = {
    val distribution = model.distributionForInstance(testExample)
    distribution(distribution.length - 1) = distribution(distribution.length - 1) * -1.0

    return distribution.toList
  }

  override def toString(): String = classifier.toString()
}
