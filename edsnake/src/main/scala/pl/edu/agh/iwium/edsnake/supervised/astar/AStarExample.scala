package pl.edu.agh.iwium.edsnake.supervised.astar

import scala.language.implicitConversions

import com.google.common.base.Preconditions.checkNotNull

import pl.edu.agh.iwium.edsnake.supervised.Example
import weka.core.Instance

class AStarExample private extends Example(4) {
}

object AStarExample {

  def builder(attributes: AStarLearningAttribiutes): ExampleBuilder = {
    return new ExampleBuilder(attributes)
  }

  implicit def snakeExampleToWekaInstance(snakeExample: AStarExample): Instance = snakeExample.example

  class ExampleBuilder(attributes: AStarLearningAttribiutes) {

    val snakeExample = new AStarExample()

    var value: java.lang.Double = _

    var distance: java.lang.Double = _

    var life: java.lang.Double = _

    var clazz: String = _

    def value(value: java.lang.Double): ExampleBuilder = {
      this.value = value
      return this
    }

    def life(life: java.lang.Double): ExampleBuilder = {
      this.life = life
      return this
    }

    def distance(distance: java.lang.Double): ExampleBuilder = {
      this.distance = distance
      return this
    }

    def clazz(clazz: String): ExampleBuilder = {
      this.clazz = clazz
      return this
    }

    private def build(): AStarExample = {
      snakeExample.setValue(attributes("distance"), distance)
      snakeExample.setValue(attributes("value"), value)
      snakeExample.setValue(attributes("life"), life)
      snakeExample.setIfNotNull(attributes("clazz"), clazz)

      return snakeExample
    }

    def buildTraining(): AStarExample = {
      checkNotNull(clazz, "%s cannot be null", "clazz")
      
      val trainingExample = build()
      attributes.addExampleToTrainingSet(trainingExample)

      return trainingExample
    }

    def buildTest: AStarExample = {
      val testExample = build()
      testExample.setDataset(attributes.trainingSet)

      return testExample
    }
  }
}