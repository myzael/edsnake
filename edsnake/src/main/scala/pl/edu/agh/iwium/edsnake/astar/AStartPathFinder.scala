package pl.edu.agh.iwium.edsnake.astar

import scala.collection.mutable.ListBuffer
import org.newdawn.slick.util.pathfinding.Path
import pl.edu.agh.iwium.edsnake.Move.DOWN
import pl.edu.agh.iwium.edsnake.Move.LEFT
import pl.edu.agh.iwium.edsnake.Move.RIGHT
import pl.edu.agh.iwium.edsnake.Move.UP
import pl.edu.agh.iwium.edsnake.Board
import pl.edu.agh.iwium.edsnake.Move
import pl.edu.agh.iwium.edsnake.Position

class AStartPathFinder(board: Board) {

  private lazy val pathFinder = board.getPathFinder()

  def findPath(position: Position): Option[List[Move]] = {
    shortestPath(position) match {
      case Some(shortestPath) => {
        val moves = toMoves(shortestPath)
        Some(moves)
      }
      case None => None
    }
  }

  private def shortestPath(end: Position): Option[Path] = {
    val player = board.getPlayerPostition()
    val distance = pathFinder.findPath(null, player.getX, player.getY, end.getX, end.getY)
    return Option(distance)
  }

  private def toMoves(path: Path): List[Move] = {
    val positions = toPosition(path)

    val moves = ListBuffer[Move]()
    for (i <- 0 until positions.size - 1) {
      val positionA = positions(i)
      val positionB = positions(i + 1)

      val x = positionB.getX - positionA.getX
      val y = positionB.getY - positionA.getY

      moves += toMove(x, y)
    }

    return moves.toList
  }

  private def toMove(x: Int, y: Int): Move = {
    if (x == 1) RIGHT else if (x == -1) LEFT else if (y == -1) UP else DOWN
  }

  private def toPosition(path: Path): List[Position] = {
    val indexes = (0 until path.getLength)
    val steps = indexes map { stepIndex =>
      val x = path.getX(stepIndex)
      val y = path.getY(stepIndex)

      new Position(x, y)
    }

    return steps.toList
  }
}