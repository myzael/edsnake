package pl.edu.agh.iwium.edsnake.supervised.directions.player

import scala.collection.mutable.HashMap
import scala.collection.mutable.ListBuffer
import scala.util.Random

import com.typesafe.scalalogging.slf4j.Logging

import pl.edu.agh.iwium.edsnake.AbstractPlayer
import pl.edu.agh.iwium.edsnake.Game
import pl.edu.agh.iwium.edsnake.IStrategy
import pl.edu.agh.iwium.edsnake.Move
import pl.edu.agh.iwium.edsnake.Position
import pl.edu.agh.iwium.edsnake.Resource
import pl.edu.agh.iwium.edsnake.ResourcesListener
import pl.edu.agh.iwium.edsnake.astar.player.AStartRandomPlayer
import pl.edu.agh.iwium.edsnake.supervised.directions.DirectionsExample.ExampleBuilder
import pl.edu.agh.iwium.edsnake.supervised.directions.DirectionsModel
import pl.edu.agh.iwium.edsnake.supervised.directions.DistributionWithMove
import pl.edu.agh.iwium.edsnake.supervised.directions.DistributionWithMove.toMove
import collection.JavaConversions._

case class DistanceRightDown(right: Int, down: Int) {

  def length() = (math.abs(right) + math.abs(down)).toInt
}

object DistanceRightDown {

  def apply(source: Position, target: Position): DistanceRightDown = {
    val (px, py) = toTuple(source)
    val (rx, ry) = toTuple(target)

    return DistanceRightDown(rx - px, ry - py)
  }

  private def toTuple(position: Position): (Int, Int) = (position.getX, position.getY)
}

class DirectionsSupervisedLearingPlayer extends AbstractPlayer {

  override def getStrategy(): IStrategy = {
    return strategy
  }

  lazy val strategy = new IStrategy with Logging with ResourcesListener {

    private val board = getBoard()

    private val timeWindow = new TimeWindow[Move](4)

    private val resources = new HashMap[Position, DistanceRightDown]()

    private val astar = new AStartRandomPlayer()

    private var move1 = new DirectionsModel("move1")

    private var move2 = new DirectionsModel("move2")

    private var move3 = new DirectionsModel("move3")

    private var move4 = new DirectionsModel("move4")

    private var target = getCloseBestResource

    private var moves: Iterator[Move] = Iterator.empty

    private var iteration = 0

    private var pointBefore = 0

    board.register(this)

    override def getMove(): Move = {
      iteration += 1

      if (moves.isEmpty && iteration >= DirectionsSupervisedLearingPlayer.Iterations - 1000 && target.isDefined) {
        moves = getNextMove(target.get)
      }

      if (moves.isEmpty && iteration < DirectionsSupervisedLearingPlayer.Iterations - 1000) {
        pointBefore = getResult()
        astar.updateBoard(board)

        moves = Iterator(astar.getStrategy.getMove())
      }

      if (moves.isEmpty) {
        moves = Iterator(Move.values()(Random.nextInt(4)))
      }

      if (iteration == DirectionsSupervisedLearingPlayer.Iterations - 1) {
        logger.debug(s"Result: ${getResult() - pointBefore}")
      }

      if (iteration == DirectionsSupervisedLearingPlayer.Iterations - 1000) {
        logger.debug(move1.toString)
        logger.debug(move2.toString)
        logger.debug(move3.toString)
        logger.debug(move4.toString)
      }
      
      val nextMove = moves.next
      timeWindow.push(nextMove)

      return nextMove
    }

    def relearn() {
      move1 = new DirectionsModel("move1")
      move2 = new DirectionsModel("move2")
      move3 = new DirectionsModel("move3")
      move4 = new DirectionsModel("move4")
    }

    private def getNextMove(target: Resource): Iterator[Move] = {
      val distance = DistanceRightDown(playerPosition, target.getPosition())

      val nextMove1Example = DirectionsModel.builder.right(distance.right).down(distance.down).value(target.getValue).clazz("+").buildTest
      val nextMove1Distribiution = move1.moveDistribution(nextMove1Example)
      val nextMove1 = maxMoveDistribiution(nextMove1Distribiution)

      val nextMove2Example = DirectionsModel.builder.right(distance.right).down(distance.down).value(target.getValue).move1(nextMove1).clazz("+").buildTest
      val nextMove2Distribiution = move2.moveDistribution(nextMove2Example)
      val nextMove2 = maxMoveDistribiution(nextMove2Distribiution)

      val nextMove3Example = DirectionsModel.builder.right(distance.right).down(distance.down).value(target.getValue).move1(nextMove1).move2(nextMove2).clazz("+").buildTest
      val nextMove3Distribiution = move3.moveDistribution(nextMove3Example)
      val nextMove3 = maxMoveDistribiution(nextMove3Distribiution)

      val nextMove4Example = DirectionsModel.builder.right(distance.right).down(distance.down).value(target.getValue).move1(nextMove1).move2(nextMove2).move3(nextMove3).clazz("+").buildTest
      val nextMove4Distribiution = move4.moveDistribution(nextMove4Example)
      val nextMove4 = maxMoveDistribiution(nextMove4Distribiution)

      return movesFilter(List(nextMove1, nextMove2, nextMove3, nextMove4)).iterator
    }

    private def movesFilter(moves: List[DistributionWithMove]): List[Move] = {
      val filtredMoves = new ListBuffer[Move]

      for (move <- moves) {
        if (move.distribution > 0.4) {
          filtredMoves += move
        } else {
          return filtredMoves.toList
        }
      }

      return filtredMoves.toList
    }

    private def maxMoveDistribiution(movesDistribiution: List[DistributionWithMove]): DistributionWithMove = {
      return movesDistribiution.maxBy(_.distribution)
    }

    private def getCloseBestResource(): Option[Resource] = {
      val resources = getBoard().getResourcesMap()
      val closeResources = resources.values().filter { r => DistanceRightDown(playerPosition, r.getPosition).length <= 4 }

      return if (closeResources.isEmpty) {
        None
      } else {
        Some(closeResources.maxBy(_.getValue()))
      }
    }

    override def resourceGenerated(resource: Resource) {
      val distance = DistanceRightDown(playerPosition, resource.getPosition)
      if (math.abs(distance.right) + math.abs(distance.down) <= 4.0) {
        resources += resource.getPosition -> DistanceRightDown(playerPosition, resource.getPosition)
      }
    }

    override def resourceExpired(resource: Resource) {
      if (target.isDefined && target.get.getPosition() == resource.getPosition()) {
        target = getCloseBestResource
      } else if (target.isEmpty) {
        target = getCloseBestResource
      }

      resources.remove(resource.getPosition)
    }

    override def resourceEaten(resource: Resource) {
      if (target.isDefined && target.get.getPosition() == resource.getPosition()) {
        target = getCloseBestResource
      } else if (target.isEmpty) {
        target = getCloseBestResource
      }

      resources.remove(resource.getPosition) match {
        case Some(distance) if resource.getValue() > 0 => generatePlus(distance, resource)
        case Some(distance) if resource.getValue() <= 0 => generateMinus(distance, resource)
        case _ =>
      }
    }

    private def generatePlus(distance: DistanceRightDown, resource: Resource) {
      val plus = generate(distance, resource)
      plus.clazz("+").buildTraining
      logger.debug("plus...")
      relearn()
    }

    private def generateMinus(distance: DistanceRightDown, resource: Resource) {
      val minus = generate(distance, resource)
      minus.clazz("-").buildTraining
      logger.debug("minus...")
      relearn()
    }

    private def generate(distance: DistanceRightDown, resource: Resource): ExampleBuilder = {
      val lastMoves = timeWindow.toList
      val (next1, next2, next3, next4) = (lastMoves(0), lastMoves(1), lastMoves(2), lastMoves(3))
      
      val sb = new StringBuilder(s"down: ${distance.down} right: ${distance.right} value: ${resource.getValue}")
      val builder = DirectionsModel.builder.down(distance.down).right(distance.right).value(resource.getValue)
      if (distance.length >= 1) {
        sb ++= s" move1: $next1"
        builder.move1(next1)
      }
//      if (distance.length >= 2) {
//        sb ++= s" move2: $next2"
//        builder.move2(next2)
//      }
//      if (distance.length >= 3) {
//        sb ++= s" move3: $next3"
//        builder.move3(next3)
//      }
//      if (distance.length >= 4) {
//        sb ++= s" move4: $next4"
//        builder.move4(next4)
//      }
      
      logger.debug(sb.toString)
      return builder
    }

    private def playerPosition(): Position = {
      return board.getPlayerPostition
    }
  }
}

object DirectionsSupervisedLearingPlayer extends App {

  private val Iterations = 5000
  Game.run(new DirectionsSupervisedLearingPlayer(), Iterations, 0)
}