package pl.edu.agh.iwium.edsnake.supervised.astar

import pl.edu.agh.iwium.edsnake.Move
import pl.edu.agh.iwium.edsnake.Resource
import pl.edu.agh.iwium.edsnake.ResourcesListener
import pl.edu.agh.iwium.edsnake.astar.AStartPathFinder

class AstarExampleGenerator(pathFinder: AStartPathFinder) extends ResourcesListener {

  override def resourceGenerated(resource: Resource) {
    val path = pathFinder.findPath(resource.getPosition)

    path match {
      case Some(path) => makeExample(resource, path)
      case None =>
    }
  }

  private def makeExample(resource: Resource, pathToResource: List[Move]) {
    val clazz = if (resource.getValue < 0) {
      "-1"
    } else if (resource.getValue == 0) {
      "2"
    } else if (resource.getLife < pathToResource.size) {
      "2"
    }  else {
      "clazz"
    }
      
    println(s"SnakeExample.builder(attr).life(${resource.getLife}).distance(${pathToResource.size}).value(${resource.getValue}).clazz(${'"' + clazz + '"'}).buildTraining")
  }

  override def resourceExpired(resource: Resource) {

  }

  override def resourceEaten(resource: Resource) {
  }
}