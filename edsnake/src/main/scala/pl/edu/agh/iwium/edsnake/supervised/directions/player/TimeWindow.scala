package pl.edu.agh.iwium.edsnake.supervised.directions.player

import scala.collection.mutable.Queue

class TimeWindow[T](maxSize: Int) {

	private val timeWindow = new Queue[T]()

	def push(element: T): TimeWindow[T] = {
		if (timeWindow.size > maxSize) {
			timeWindow.dequeue
		}

		timeWindow += element

		return this
	}

	def toList(): List[T] = {
		return timeWindow.toList.reverse
	}
}