package pl.edu.agh.iwium.edsnake.supervised.directions

import scala.language.implicitConversions
import com.google.common.base.Preconditions.checkNotNull
import pl.edu.agh.iwium.edsnake.supervised.Example
import weka.core.Instance
import weka.core.Attribute
import pl.edu.agh.iwium.edsnake.Move
import pl.edu.agh.iwium.edsnake.Move._

class DirectionsExample private extends Example(8) {

  def setIfNotNull(attribute: Attribute, value: Move) {
    if (value != null) {
      setValue(attribute, value.toString)
    }
  }
}

object DirectionsExample {

  def builder(attributes: DirectionsLearningAttribiutes): ExampleBuilder = {
    return new ExampleBuilder(attributes)
  }

  implicit def snakeExampleToWekaInstance(snakeExample: DirectionsExample): Instance = snakeExample.example

  class ExampleBuilder(attributes: DirectionsLearningAttribiutes) {

    val snakeExample = new DirectionsExample()

    var right: java.lang.Double = _

    var down: java.lang.Double = _

    var move1: Move = _

    var move2: Move = _

    var move3: Move = _

    var move4: Move = _

    var value: Int = _

    var clazz: String = _

    def right(right: java.lang.Double): ExampleBuilder = {
      this.right = right
      return this
    }

    def down(down: java.lang.Double): ExampleBuilder = {
      this.down = down
      return this
    }

    def move1(move1: Move): ExampleBuilder = {
      this.move1 = move1
      return this
    }

    def move2(move2: Move): ExampleBuilder = {
      this.move2 = move2
      return this
    }

    def move3(move3: Move): ExampleBuilder = {
      this.move3 = move3
      return this
    }

    def move4(move4: Move): ExampleBuilder = {
      this.move4 = move4
      return this
    }

    def clazz(clazz: String): ExampleBuilder = {
      this.clazz = clazz
      return this
    }

    def value(value: Int): ExampleBuilder = {
      this.value = value
      return this
    }

    private def build(): DirectionsExample = {
      snakeExample.setValue(attributes("right"), right)
      snakeExample.setValue(attributes("down"), down)
      snakeExample.setValue(attributes("value"), value)
      snakeExample.setIfNotNull(attributes("move1"), move1)
      snakeExample.setIfNotNull(attributes("move2"), move2)
      snakeExample.setIfNotNull(attributes("move3"), move3)
      snakeExample.setIfNotNull(attributes("move4"), move4)
      snakeExample.setIfNotNull(attributes("clazz"), clazz)

      return snakeExample
    }

    def buildTraining(): DirectionsExample = {
      checkNotNull(clazz, "%s cannot be null", "clazz")

      val trainingExample = build()
      attributes.addExampleToTrainingSet(trainingExample)

      return trainingExample
    }

    def buildTest: DirectionsExample = {
      val testExample = build()
      testExample.setDataset(attributes.trainingSet)

      return testExample
    }
  }
}