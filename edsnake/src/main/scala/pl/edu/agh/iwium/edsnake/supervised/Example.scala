package pl.edu.agh.iwium.edsnake.supervised

import com.google.common.base.Preconditions.checkNotNull

import weka.core.Attribute
import weka.core.Instance

abstract class Example(attribiutesCount: Int) {
  
  protected val example = new Instance(attribiutesCount)

  def setValue(attribute: Attribute, value: Double) {
    checkNotNull(value, "%s cannot be null", attribute.name())
    example.setValue(attribute, value)
  }

  def setValue(attribute: Attribute, value: String) {
    checkNotNull(value, "%s cannot be null", attribute.name())
    example.setValue(attribute, value)
  }

  def setIfNotNull(attribute: Attribute, value: java.lang.Double) {
    if (value != null) {
      setValue(attribute, value)
    }
  }

  def setIfNotNull(attribute: Attribute, value: String) {
    if (value != null) {
      setValue(attribute, value)
    }
  }
}